package com.example.pavankc.isspasstimes.data;

import com.example.pavankc.isspasstimes.model.Response;

import java.util.ArrayList;

public interface IssPassTimesDataSource {

    interface GetIssPassTimesCallBack {

        void onIssPassTimesLoaded(ArrayList<Response> responses);

        void onDataNotAvailable();

    }

    void getIssPassTimes(double lat, double lon, GetIssPassTimesCallBack callBack);
}
