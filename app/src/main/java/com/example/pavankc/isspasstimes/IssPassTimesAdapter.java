package com.example.pavankc.isspasstimes;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.pavankc.isspasstimes.model.Response;
import com.example.pavankc.isspasstimes.utilities.DateHelper;

import java.util.ArrayList;

public class IssPassTimesAdapter extends RecyclerView.Adapter<IssPassTimesAdapter.ViewHolder>{

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView mDateTextView;
        public TextView mDurationTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            mDateTextView = itemView.findViewById(R.id.date_tv);
            mDurationTextView = itemView.findViewById(R.id.duration_tv);
        }
    }

    private ArrayList<Response> mReponses;
    private Context mContext;

    // Pass in the responses array into the constructor
    public IssPassTimesAdapter(Context context, ArrayList<Response> responses) {
        mReponses = responses;
        mContext = context;
    }

    @Override
    public IssPassTimesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View PassesItemView = inflater.inflate(R.layout.item_layout, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(PassesItemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(IssPassTimesAdapter.ViewHolder holder, int position) {
        // Get the data model based on position
        Response response = mReponses.get(position);

        // Set item views based on your views and data model
        String dateString = String.valueOf(response.getRisetime());
        String titleDateString = DateHelper.getDateString(dateString);
        holder.mDateTextView.setText(titleDateString);

        String durationString = String.format("Visible for %s seconds", String.valueOf(response.getDuration()));
        holder.mDurationTextView.setText(durationString);
    }

    @Override
    public int getItemCount() {
        return mReponses.size();
    }
}
