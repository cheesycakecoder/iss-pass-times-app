package com.example.pavankc.isspasstimes;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class IssPassTimesActivity extends AppCompatActivity {

    private static final String TAG = IssPassTimesActivity.class.getSimpleName();
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 1001;

    private IssPassTimesPresenter mPresenter;

    private View mLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iss_pass_times);

        mLayout = findViewById(R.id.main_layout);

        //Fragment Code
        IssPassTimesListFragment issPassListFragment =
                (IssPassTimesListFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrame);
        if (issPassListFragment == null) {
            // Create the fragment
            issPassListFragment = IssPassTimesListFragment.newInstance();

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.contentFrame, issPassListFragment)
                    .commit();
        }

        // Create the presenter
        mPresenter = new IssPassTimesPresenter(this, issPassListFragment);

        requestLocationPermission();
    }

    private void requestLocationPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                MY_PERMISSIONS_REQUEST_LOCATION);
        return;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay!
                    Log.i(TAG, "LOCATION permission has now been granted");

                    //Loading data on first Permission grant
                    mPresenter.loadIssPassTimesList();

                } else if(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_DENIED){
                    if( ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.ACCESS_COARSE_LOCATION)); {

                        //Permission Explanantion
                        Snackbar.make(mLayout, "Hey, Astronaut ! Location access required to display ISS passing overhead",
                                Snackbar.LENGTH_INDEFINITE).setAction("OK", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Request the permission
                                requestLocationPermission();
                            }
                        }).show();

                    }
                } else {
                    // never ask again?, boo! use default coordinates
                    Log.d(TAG, "LOCATION permission was NOT granted.");
                }
                return;
            }
        }
    }

}
