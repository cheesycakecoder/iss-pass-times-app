package com.example.pavankc.isspasstimes.utilities;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateHelper {

    // Returns in format e.g. Sunday, December 3
    public static String getDateString(String epochString){

        String weekName;
        String monthName;
        String day;
        String time;

        Date convertedDate = new Date(Long.parseLong(epochString) * 1000);
        Calendar c = Calendar.getInstance();
        c.setTime(convertedDate);

        SimpleDateFormat weekFormat = new SimpleDateFormat( "EEEE", Locale.getDefault() );
        SimpleDateFormat monthFormat = new SimpleDateFormat( "MMMM", Locale.getDefault() );
        SimpleDateFormat dayFormat = new SimpleDateFormat("d", Locale.getDefault());
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm:ss a");

        weekName = weekFormat.format(c.getTime());
        monthName = monthFormat.format(c.getTime());
        day = dayFormat.format(c.getTime());
        time = timeFormat.format(c.getTime());

        String titleString = new StringBuilder().append(weekName)
                .append(", ")
                .append(monthName)
                .append(" ")
                .append(day)
                .append("\n")
                .append(time)
                .toString();

        return titleString;
    }

}
