package com.example.pavankc.isspasstimes;

import android.content.Context;

import com.example.pavankc.isspasstimes.data.IssPassTimesDataSource;
import com.example.pavankc.isspasstimes.data.remote.IssPassTimesRemoteDataSource;
import com.example.pavankc.isspasstimes.location.LocationManagerApi;
import com.example.pavankc.isspasstimes.model.Response;

import java.util.ArrayList;
import java.util.List;

public class IssPassTimesPresenter implements IssPassTimesContract.Presenter, LocationManagerApi.LocationResultCallBack {


    public static final String TAG = IssPassTimesPresenter.class.getSimpleName();

    private Context mContext;
    private final IssPassTimesContract.View mView;
    private LocationManagerApi locationManagerApi;

    public IssPassTimesPresenter(Context context, IssPassTimesContract.View view){
        mContext = context;
        mView = view;
        mView.setPresenter(this);
        locationManagerApi = new LocationManagerApi(context, this);
    }

    public void loadIssPassTimesList(){

        //Default location coordinates for now.
        double lat = 41.875970;
        double lon = -87.620256;

        locationManagerApi.getOneTimeLocation();

//
//        IssPassTimesRemoteDataSource.getInstance().getIssPassTimes(lat, lon, new IssPassTimesDataSource.GetIssPassTimesCallBack() {
//
//            @Override
//            public void onIssPassTimesLoaded(List<Response> responses) {
//                mView.setAdapter(responses);
//            }
//
//            @Override
//            public void onDataNotAvailable() {
//
//            }
//        });
    }

    @Override
    public void onLocationResult(double lat, double lon) {
        IssPassTimesRemoteDataSource.getInstance().getIssPassTimes(lat, lon, new IssPassTimesDataSource.GetIssPassTimesCallBack() {

            @Override
            public void onIssPassTimesLoaded(ArrayList<Response> responses) {
                mView.setAdapter(responses);
            }

            @Override
            public void onDataNotAvailable() {

            }
        });
    }
}
