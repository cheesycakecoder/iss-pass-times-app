package com.example.pavankc.isspasstimes.data.remote;

import android.util.Log;

import com.example.pavankc.isspasstimes.data.IssPassTimesDataSource;
import com.example.pavankc.isspasstimes.model.ApiResponseData;
import com.example.pavankc.isspasstimes.network.RetrofitClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IssPassTimesRemoteDataSource implements IssPassTimesDataSource{

    private static final String TAG = IssPassTimesRemoteDataSource.class.getSimpleName();

    private static IssPassTimesRemoteDataSource INSTANCE;

    private final RetrofitClient mRetrofitClient;

    public static IssPassTimesRemoteDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new IssPassTimesRemoteDataSource();
        }
        return INSTANCE;
    }

    // Prevent direct instantiation.
    private IssPassTimesRemoteDataSource() {mRetrofitClient = new RetrofitClient();}

    @Override
    public void getIssPassTimes(double lat, double lon, final GetIssPassTimesCallBack callBack) {

        //Default next 20 passes for now
        int n = 20;

        //Retrofit Call
        Call<ApiResponseData> call = mRetrofitClient.getApi().getIssPass(lat, lon, n);

        call.enqueue(new Callback<ApiResponseData>() {
            @Override
            public void onResponse(Call<ApiResponseData> call, Response<ApiResponseData> response) {
                Log.d(TAG, "REQUEST SUCCESS " + response);

                int statusCode = response.code();

                ApiResponseData apiResponseData = response.body();

                if(apiResponseData != null){
                    ArrayList<com.example.pavankc.isspasstimes.model.Response> responseData =
                            apiResponseData.getResponse();

                    callBack.onIssPassTimesLoaded(responseData);
                }
            }

            @Override
            public void onFailure(Call<ApiResponseData> call, Throwable t) {
                // Log error here since request failed
                Log.d(TAG, "REQUEST FAILED");
                callBack.onDataNotAvailable();
            }
        });
    }
}
