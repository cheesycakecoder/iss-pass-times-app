package com.example.pavankc.isspasstimes.location;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

public class LocationManagerApi {

    public static final String TAG = LocationManagerApi.class.getSimpleName();

    private Context mContext;

    private FusedLocationProviderClient mFusedLocationClient;

    private LocationResultCallBack mCallBack;

    public LocationManagerApi(Context mContext, LocationResultCallBack callBack) {
        this.mContext = mContext;
        mCallBack = callBack;
    }

    public void getOneTimeLocation() {

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(mContext);

        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG, "Location permissions has NOT been granted. Requesting permissions.");
            // TODO - Not requesting permissions here Hoping permissions we're already granted
//            requestLocationPermission();

            return;
        }

        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener((Activity) mContext, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object
                            double latitude = location.getLatitude();
                            double longitude = location.getLongitude();
                            Log.d(TAG, "I'm here  " + latitude + " , " + longitude);
                            mCallBack.onLocationResult(latitude, longitude);
                        }
                    }
                });

    }

    public interface LocationResultCallBack {

        void onLocationResult(double lat, double lon);
    }

}
