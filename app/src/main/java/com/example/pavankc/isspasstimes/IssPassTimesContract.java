package com.example.pavankc.isspasstimes;

import com.example.pavankc.isspasstimes.model.Response;

import java.util.ArrayList;

public class IssPassTimesContract {

    interface View {

        void setPresenter(Presenter presenter);

        void setAdapter(ArrayList<Response> responses);

    }

    interface Presenter {

        void loadIssPassTimesList();
    }
}
