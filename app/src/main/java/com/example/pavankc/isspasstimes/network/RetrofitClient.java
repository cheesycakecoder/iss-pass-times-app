package com.example.pavankc.isspasstimes.network;

import com.example.pavankc.isspasstimes.model.ApiResponseData;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class RetrofitClient {

    //REST API URL --> http://api.open-notify.org/iss-pass.json?lat=LAT&lon=LON
    public static final String BASE_URL = "http://api.open-notify.org/";

    public MyApiEndpointInterface getApi() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(MyApiEndpointInterface.class);
    }

    public interface MyApiEndpointInterface {

        @GET("iss-pass.json")
        Call<ApiResponseData> getIssPass(@Query("lat") double lat,
                                         @Query("lon") double lon,
                                         @Query("n") int n);
    }
}
