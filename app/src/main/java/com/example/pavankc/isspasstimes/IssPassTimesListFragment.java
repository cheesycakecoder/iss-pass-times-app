package com.example.pavankc.isspasstimes;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.pavankc.isspasstimes.model.Response;

import java.util.ArrayList;
import java.util.List;


public class IssPassTimesListFragment extends Fragment implements IssPassTimesContract.View {

    private static final String TAG = IssPassTimesListFragment.class.getSimpleName();

    private IssPassTimesContract.Presenter mPresenter;

    private RecyclerView mRecyclerView;
    private IssPassTimesAdapter mAdapter;

    private ArrayList<Response> mResponses;

    public static IssPassTimesListFragment newInstance() {
        return new IssPassTimesListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_iss_pass_list, container, false);

        mRecyclerView = root.findViewById(R.id.iss_pass_list_rv);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mPresenter.loadIssPassTimesList();

        return root;
    }

    @Override
    public void setPresenter(IssPassTimesContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void setAdapter(ArrayList<Response> responses) {
        mResponses = responses;
        mAdapter = new IssPassTimesAdapter(getActivity(), responses);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(savedInstanceState != null){
            mResponses = savedInstanceState.getParcelableArrayList("D");
            if(mResponses != null){
                setAdapter(mResponses);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("D", mResponses);
    }
}
