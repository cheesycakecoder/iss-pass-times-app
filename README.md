# Coding Challenge: International Space Station Passes

## GOAL: Verify candidate can provide a technical solution and follow instructions.

Implemented the core requirements
REST API Call, Device Location , List of ISS Sightings with duration.
:sweat_drops:

There is definitely room for improvement ! :raised_hands:

### Author

**Pavan Kumar**